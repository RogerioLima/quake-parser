
#Quake Log Parser

# Requerimentos
  * Java 8

# Ambiente de Desenvolvimento
  * IDE Eclipse Oxygen
  * Maven
  * Framework Spring Boot
  * JDK 8
  * Teste de saida com SoapUI 5.3

# Setup
  * Execute a classe QuakeLogParserApplication (./src/main/java/br/com/lima/quakelogparser/QuakeLogParserApplication.java) como Java Application
  * Após a aplicação ser carregada, a mesma poderá ser acessada por localhost:8080/games
  * O arquivo de log a ser analisado é o games.log e será carregado diretamente do diretório ./src/main

# Documentação
  * Disponível em ./src/main/doc

# Operações disponíveis
  * localhost:8080/games/todos - lista todos os games disponíveis
  * localhost:8080/games/game/id - busca por um game específico identificado por um id (número inteiro)

# Solução
  * É Feito o carregamento do arquivo games.log e armazenado suas linhas em um List<String>
  * Na primeira vez que um recurso for acionado, será feito o processamento desta lista e o resultado é armazenado em um List<Game>. A partir de então, só será feito um novo processamento da lista se houver alguma alteração no arquivo games.log ou no caso da aplicação ser reiniciada.
  * O processamento é feito percorrendo a List<String> e verificando se a linha corresponde a:
	inicio de jogo - InitGame:
	player - ClientUserinfoChanged:
	kill - kill:
  * Encontrando um desses identificadores, é utilizado expressão regular para retirar as informações da linha.
  * Ao terminar o processamento todas as informações terão sido salvas em um List<Game>, onde cada objeto Game contém todas as informações da partida.
  * Ao acionar um recurso, a classe GameService é chamada. Ela pegará um Game e formatará suas informações para devolver ao requisitante.
  * As informações de saida de cada Game são:
	total_kills - é a quantidade de mortes ocorridas na partida.
	players - nome dos players participantes
	kills - quantidade de vezes que um player matou outro (essa quantidade pode ser negativa, pois a cada vez que um player é morto pelo <world>, ele perde -1 kill.
	suicides - quantidade de vezes que um player cometeu suicidio.
	causes_of_death - quantidade de vezes que uma morte foi causada por uma determinada arma.
