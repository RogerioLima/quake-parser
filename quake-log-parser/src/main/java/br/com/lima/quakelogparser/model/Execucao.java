package br.com.lima.quakelogparser.model;

import java.io.Serializable;

/**
 * 
 * @author Rogério Lima
 *
 */
public class Execucao implements Serializable {

	private static final long serialVersionUID = 1L;

	private String quemMorreu;
	private String arma;

	public Execucao() {
		super();
	}

	public Execucao(String quemMorreu) {
		super();
		this.quemMorreu = quemMorreu;
	}

	public Execucao(String quemMorreu, String arma) {
		super();
		this.quemMorreu = quemMorreu;
		this.arma = arma;
	}

	public String getQuemMorreu() {
		return quemMorreu;
	}

	public void setQuemMorreu(String quemMorreu) {
		this.quemMorreu = quemMorreu;
	}

	public String getArma() {
		return arma;
	}

	public void setArma(String arma) {
		this.arma = arma;
	}

}
