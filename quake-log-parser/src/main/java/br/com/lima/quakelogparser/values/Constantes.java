package br.com.lima.quakelogparser.values;

import java.util.regex.Pattern;

/**
 * Constantes públicas utilizadas no sistema
 * 
 * @author Rogério Lima
 *
 */
public class Constantes {

	/**
	 * Padrão presente nas linhas que indicam o início de um Game
	 */
	public static final String INIT_GAME = "InitGame:";
	
	/**
	 * Padrão presente nas linhas que indicam a existencia de um player<br>
	 * Quando um player se conecta ao jogo é atribuido a ele um id. <br>
	 * Esse id continua com o player até o fim do jogo ou até ele ser desconectado. <br>
	 */
	public static final String USER_INFO_CHANGED = "ClientUserinfoChanged:";
	
	/**
	 * Padrão presente nas linhas que indicam a ocorrencia de uma Kill
	 */
	public static final String KILL = "Kill:";
	
	/**
	 * Padrão presente nas linhas que indicam quando um player é desconetado
	 */
	public static final String CLIENT_DISCONNECT = "ClientDisconnect:";
	
	/**
	 * Padrão presente nas linhas que indicam atividade do mundo do jogo <br>
	 * <p>Ele não é um player em si, mas pode causar a morte de um player. <br>
	 * <p>Por exemplo: um abismo presente no cenário do jogo causa a morte de um player que cai nesse abismo.
	 * Nesse caso a morte foi causada pelo &lt;world&gt;.
	 */
	public static final String WORLD = "<world>";
	
	/**
	 * Expressão regular utilizada para retirar as informações do player da linha lida de um arquivo.<br>
	 * <p>Se o padrão estiver na linha, ele armazenará:<br>
	 * * o "id" no grupo 1<br>
	 * * o nome do player no grupo 2.
	 */
	public static final Pattern PATTERN_PLAYER = Pattern.compile(".*" + USER_INFO_CHANGED + " ([0-9]+) n\\\\(.+)\\\\t\\\\.*");
	
	/**
	 * Expressão regular utilizada para retirar as informações de morte da linha lida de um arquivo.
	 * <p>Se o padrão estiver na linha, ele armazenará:<br>
	 * * nome do player que matou ou &lt;world&gt; se a morte tiver sido causada pelo world no grupo 2 <br>
	 * * nome do player que morreu no grupo 3 <br>
	 * * nome da arma utilizada no grupo 4.
	 */
	public static final Pattern PATTERN_KILL = Pattern.compile(".*" + KILL + " ([0-9]+ [0-9]+ [0-9]+): (.+) killed (.+) by (.+)");
	
	/**
	 * Expressão regular utilizada para retirar da linha o "id" do player no momento da desconexão.
	 * <p>Se o padrão estiver na linha, ele armazenará:<br>
	 * * o "id" no grupo 1
	 */
	public static final Pattern PATTERN_DISCONNECT = Pattern.compile(".*" + CLIENT_DISCONNECT + " ([0-9]+)");
	
	/**
	 * Código atribuido ao player no momento em que ele é desconectado
	 */
	public static final int COD_DESCONECTADO = -1;
}
