package br.com.lima.quakelogparser.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Rogério Lima
 *
 */
public class Game implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String GAME = "game";

	private int id;
	private int totalKills;
	private String nome;
	private List<Player> players;
	private Map<String, Integer> causasDeMorte;

	public Game() {
		players = new ArrayList<>();
		causasDeMorte = new HashMap<>();
	}

	public Game(int id) {
		super();
		this.id = id;
		players = new ArrayList<>();
		causasDeMorte = new HashMap<>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTotalKills() {
		return totalKills;
	}

	public void setTotalKills(int totalKills) {
		this.totalKills = totalKills;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public String getNome() {
		if (nome == null || nome.isEmpty()) {
			return GAME + "_" + id;
		}
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Map<String, Integer> getCausasDeMorte() {
		return causasDeMorte;
	}

	public void setCausasDeMorte(Map<String, Integer> causasDeMorte) {
		this.causasDeMorte = causasDeMorte;
	}

}
