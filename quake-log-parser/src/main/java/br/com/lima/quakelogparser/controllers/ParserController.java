package br.com.lima.quakelogparser.controllers;

import static br.com.lima.quakelogparser.values.Constantes.CLIENT_DISCONNECT;
import static br.com.lima.quakelogparser.values.Constantes.COD_DESCONECTADO;
import static br.com.lima.quakelogparser.values.Constantes.INIT_GAME;
import static br.com.lima.quakelogparser.values.Constantes.KILL;
import static br.com.lima.quakelogparser.values.Constantes.PATTERN_DISCONNECT;
import static br.com.lima.quakelogparser.values.Constantes.PATTERN_KILL;
import static br.com.lima.quakelogparser.values.Constantes.PATTERN_PLAYER;
import static br.com.lima.quakelogparser.values.Constantes.USER_INFO_CHANGED;
import static br.com.lima.quakelogparser.values.Constantes.WORLD;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import br.com.lima.quakelogparser.files.CarregaArquivo;
import br.com.lima.quakelogparser.model.Execucao;
import br.com.lima.quakelogparser.model.Game;
import br.com.lima.quakelogparser.model.Kill;
import br.com.lima.quakelogparser.model.Player;


/**
 * Esta classe faz o processamento das informações dos Games. <br><br>
 * Deve receber estas informações através de um List&lt;String&gt;, onde cada posição da lista armazena uma linha distinta.
 * 
 * <p>O principal método desta classe é o método processaGames().<br>
 * Ele percorre a lista fornecida no construtor identificando se a linha é inicio de jogo, se é referente a um player, 
 * se é referente a uma desconexão ou se é referente a uma kill.
 * 
 * <p>No final do processamento ele retorna um List&lt;Game&gt;.
 * 
 * <p>A List&lt;Game&gt; é armazenada na variável estática games.<br>
 * O processamento de todas as linhas só será executado novamente se houver alguma alteração no arquivo de origem.
 * 
 * @author Rogério Lima
 *
 */
public class ParserController {
	
	private static final String NAO_PODE_SER_NULO = "O parâmetro linhas não pode ser nulo!";
	
	private List<String> linhas;
	private static List<Game> games;
	private static long tamanhoDoArquivoCarregado;
	
	public ParserController(List<String> linhas) {
		super();
		if (linhas == null) {
			throw new IllegalArgumentException(NAO_PODE_SER_NULO);
		}
		this.linhas = linhas;
	}

	/**
	 * Verifica se houve alguma alteração no arquivo de origem.
	 * <p>Se houve alteração, o processamento é executado novamente percorrendo todas as linhas do arquivo
	 * <p>Senão será utilizada a lista gerada pelo processamento anterior
	 * @return true se for necessário executar o processamento novamente<br />
	 * false caso contrário
	 * @throws IOException
	 */
	private boolean processarGames() throws IOException {
		if (games == null || tamanhoDoArquivoCarregado != Files.size(CarregaArquivo.getLocalDoArquivoCarregado())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Verifica se a linha indica início de jogo
	 * @param linha
	 * @return true se for início de jogo ou false caso contrário
	 */
	public boolean isInicioDeJogo(String linha) {
		if (linha == null) return false;
		if (linha.contains(INIT_GAME)) return true;
		return false;
	}

	/**
	 * Verifica se a linha é referente a um player
	 * @param linha
	 * @return true se for player ou false caso contrário
	 */
	public boolean isPlayer(String linha) {
		if (linha == null) return false;
		if (linha.contains(USER_INFO_CHANGED)) return true;
		return false;
	}

	/**
	 * Verifica se a linha é referente a uma kill
	 * @param linha
	 * @return true se for kill ou false caso contrário
	 */
	public boolean isKill(String linha) {
		if (linha == null) return false;
		if (linha.contains(KILL)) return true;
		return false;
	}
	
	/**
	 * Verifica se a linha é referente a uma desconexão
	 * @param linha
	 * @return true se for desconexão ou false caso contrário
	 */
	public boolean isDisconnect(String linha) {
		if (linha == null) return false;
		if (linha.contains(CLIENT_DISCONNECT)) return true;
		return false;
	}

	/**
	 * Separa os dados referentes ao player e os coloca em um objeto Player
	 * @param linhaPlayer
	 * @return Objeto Player ou null caso não encontre os dados
	 */
	public Player retiraPlayerDaLinha(String linhaPlayer) {
		Matcher matcher = PATTERN_PLAYER.matcher(linhaPlayer);
		if (!matcher.matches()) {
			return null;
		}
		int id = Integer.parseInt(matcher.group(1));
		String nome = matcher.group(2);
		return new Player(id, nome);
	}

	/**
	 * Separa os dados referentes a Kill e os coloca em um objeto Kill
	 * @param linhaKill
	 * @return Objeto Kill ou null caso não encontre os dados
	 */
	public Kill retiraKillDaLinha(String linhaKill) {
		Matcher matcher = PATTERN_KILL.matcher(linhaKill);
		if (!matcher.matches()) {
			return null;
		}
		String quemMatou = matcher.group(2);
		String quemMorreu = matcher.group(3);
		String armaUsada = matcher.group(4);
		return new Kill(quemMatou, quemMorreu, armaUsada);
	}

	/**
	 * Separa o id do player que foi desconectado
	 * @param linhaDisconnect
	 * @return o id que o player usava no momento da desconexão<br>ou -1 caso não encontre
	 */
	public int retiraIdDesconectadoDaLinha(String linhaDisconnect) {
		Matcher matcher = PATTERN_DISCONNECT.matcher(linhaDisconnect);
		if (!matcher.matches()) {
			return -1;
		}
		String idNaLinha = matcher.group(1);
		return Integer.parseInt(idNaLinha);
	}
	
	/**
	 * Percorre as linhas do arquivo extraindo as informações de cada Jogo
	 * @return List&lt;Game&gt; - nunca NULL
	 * @throws IOException 
	 */
	public List<Game> processaGames() throws IOException {
		if (!processarGames()) {
			return games;
		}
		games = new ArrayList<>();
		Game game = null;
		int qtdGames = 0;
		for (int i = 0; i < linhas.size(); i++) {
			String linha = linhas.get(i);
			if (isInicioDeJogo(linha)) {
				game = new Game(++qtdGames);
				games.add(game);
				continue;
			}
			if (isPlayer(linha) && game != null) {
				Player player = retiraPlayerDaLinha(linha);
				atualizaIdOuInserePlayerNoGame(game, player);
				continue;
			}
			if (isDisconnect(linha) && game != null) {
				int id = retiraIdDesconectadoDaLinha(linha);
				desconectarPlayer(game, id);
				continue;
			}
			if (isKill(linha) && game != null) {
				Kill kill = retiraKillDaLinha(linha);
				processaKill(game, kill);
			}
			
		}
		tamanhoDoArquivoCarregado = Files.size(CarregaArquivo.getLocalDoArquivoCarregado());
		return games;
	}

	/**
	 * Atribui as informações da Kill ao game e seus players<br>
	 * Por exemplo, verifica quem matou quem e qual arma foi utilizada, e atribui essas informações ao respectivo player,
	 * ou então se o player se suicidou ou ainda se o player foi morto pelo world.
	 * Verifica também a causa da morte, atribuindo a quantidade da causa ao game
	 * @param game
	 * @param kill
	 */
	public void processaKill(Game game, Kill kill) {
		Player player;
		String quemMatou = kill.getQuemMatou();
		String quemMorreu = kill.getQuemMorreu();
		String armaUsada = kill.getArmaUsada();
		Map<String, Integer> causasDeMorte = game.getCausasDeMorte();
		
		int totalKills = game.getTotalKills();
		game.setTotalKills(++totalKills);
		
		if (causasDeMorte.containsKey(armaUsada)) {
			int qtdVezesUsada = causasDeMorte.get(armaUsada);
			causasDeMorte.put(armaUsada, ++qtdVezesUsada);
		} else {
			causasDeMorte.put(armaUsada, 1);
		}
		
		if (WORLD.equals(quemMatou)) {
			player = buscaPlayer(game, quemMorreu);
			int qtd = player.getMorriPeloWorld();
			player.setMorriPeloWorld(++qtd);
			return;
		}
		
		player = buscaPlayer(game, quemMatou);
		
		if (quemMatou.equals(quemMorreu)) {
			int qtd = player.getSuicidio();
			player.setSuicidio(++qtd);
			return;
		}
		
		player.getQuemMatei().add(new Execucao(quemMorreu, armaUsada));
		
	}
	
	/**
	 * Encontra um player dentro do game pelo nome do player
	 * @param game - onde deve procurar
	 * @param nomePlayer - nome a ser localizado
	 * @return Player se o nome for encontrado ou null caso contrário
	 */
	private Player buscaPlayer(Game game, String nomePlayer) {
		for (Player player : game.getPlayers()) {
			try {
				if (player.getNome().equals(nomePlayer)) {
					return player;
				}
			} catch (Exception e) {
			}
		}
		return null;
	}
	
	/**
	 * Atualiza o id do player ou inseri o próprio player no jogo<br>
	 * Se o player já estiver no jogo, apenas seu id será alterado, caso contrário ele será inserido no jogo
	 * @param game - onde o player será procurado
	 * @param player - player a ser verificado se já está no jogo
	 */
	public void atualizaIdOuInserePlayerNoGame(Game game, Player player) {
		int index = game.getPlayers().indexOf(player);
		if (index == -1) {
			game.getPlayers().add(player);
			return;
		}
		Player playerNaLista = game.getPlayers().get(index);
		playerNaLista.setId(player.getId());
	}
	
	/**
	 * Atribui o id -1 ao player que é desconectado.<br>
	 * Quando um player é desconectado, ele pode se reconectar, porém o id que ele estava usando já pode ter sido
	 * designado para outro player, por isso ele recebe o id -1 até se reconectar.
	 * @param game - do qual o player foi desconectado
	 * @param id - que o player usava no momento da desconexão
	 */
	public void desconectarPlayer(Game game, int id) {
		for (Player player: game.getPlayers()) {
			if (id == player.getId()) {
				player.setId(COD_DESCONECTADO);
				break;
			}
		}
	}

	public static List<Game> getGames() {
		return games;
	}

	public static void setGames(List<Game> games) {
		ParserController.games = games;
	}

	public static long getTamanhoDoArquivoCarregado() {
		return tamanhoDoArquivoCarregado;
	}

	public static void setTamanhoDoArquivoCarregado(long tamanhoDoArquivoCarregado) {
		ParserController.tamanhoDoArquivoCarregado = tamanhoDoArquivoCarregado;
	}
	
}
