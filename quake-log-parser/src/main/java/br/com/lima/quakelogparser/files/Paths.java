package br.com.lima.quakelogparser.files;

/**
 * Locais físicos dos arquivos utilizados
 * 
 * @author Rogério Lima
 *
 */
public class Paths {

	public static String pathLog = "./src/main/games.log";
	public static String pathLogTest = "./src/test/games-test.log";
	public static String pathLogTestSemGames = "./src/test/games-test-sem-games.log";
}
