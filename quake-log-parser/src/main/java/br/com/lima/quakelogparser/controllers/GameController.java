package br.com.lima.quakelogparser.controllers;

import java.io.IOException;
import java.util.List;

import br.com.lima.quakelogparser.model.Game;

/**
 * 
 * @author Rogério Lima
 *
 */
public class GameController {

	private List<String> linhas;
	
	public GameController(List<String> linhas) {
		super();
		this.linhas = linhas;
	}

	public List<Game> listaTodos() throws IOException {
		return new ParserController(linhas).processaGames();
	}
	
	public Game bucaGame(int id) throws IOException {
		List<Game> games = new ParserController(linhas).processaGames();
		for (Game game : games) {
			if (id == game.getId()) {
				return game;
			}
		}
		return null;
	}
}
