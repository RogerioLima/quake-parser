package br.com.lima.quakelogparser.resources.exceptions;

/**
 * 
 * @author Rogério Lima
 *
 */
public class NaoEncontradoException extends Exception {

	private static final long serialVersionUID = 1L;

	public NaoEncontradoException() {
		super();
	}

	public NaoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	public NaoEncontradoException(String message) {
		super(message);
	}

	public NaoEncontradoException(Throwable cause) {
		super(cause);
	}
}
