package br.com.lima.quakelogparser.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Rogério Lima
 *
 */
public class Player implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idDuranteJogo;
	private int morriPeloWorld;
	private int suicidio;
	private String nome;
	private List<Execucao> quemMatei;

	public Player() {
		quemMatei = new ArrayList<>();
	}

	public Player(int id, String nome) {
		super();
		this.idDuranteJogo = id;
		this.nome = nome;
		this.quemMatei = new ArrayList<>();
	}

	public Player(int id, int morriPeloWorld, String nome, List<Execucao> quemMatei) {
		super();
		this.idDuranteJogo = id;
		this.morriPeloWorld = morriPeloWorld;
		this.nome = nome;
		this.quemMatei = quemMatei;
		this.quemMatei = new ArrayList<>();
	}

	public int getId() {
		return idDuranteJogo;
	}

	public void setId(int id) {
		this.idDuranteJogo = id;
	}

	public int getMorriPeloWorld() {
		return morriPeloWorld;
	}

	public void setMorriPeloWorld(int morriPeloWorld) {
		this.morriPeloWorld = morriPeloWorld;
	}

	public int getSuicidio() {
		return suicidio;
	}

	public void setSuicidio(int suicidio) {
		this.suicidio = suicidio;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Execucao> getQuemMatei() {
		return quemMatei;
	}

	public void setQuemMatei(List<Execucao> quemMatei) {
		this.quemMatei = quemMatei;
	}

	/**
	 * Calcula a quantidade de mortes executadas pelo player
	 * 
	 * @return a quantidade de mortes descontando a quantidade de vezes que ele
	 *         morreu pelo world
	 */
	public int getQuantidadeMortes() {
		return quemMatei.size() - morriPeloWorld;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}

}
