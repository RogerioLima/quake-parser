package br.com.lima.quakelogparser.model;

import java.io.Serializable;

/**
 * 
 * @author Rogério Lima
 *
 */
public class Kill implements Serializable {

	private static final long serialVersionUID = 1L;

	private String quemMatou;
	private String quemMorreu;
	private String armaUsada;

	public Kill() {
		super();
	}

	public Kill(String quemMatou, String quemMorreu, String armaUsada) {
		super();
		this.quemMatou = quemMatou;
		this.quemMorreu = quemMorreu;
		this.armaUsada = armaUsada;
	}

	public String getQuemMatou() {
		return quemMatou;
	}

	public void setQuemMatou(String quemMatou) {
		this.quemMatou = quemMatou;
	}

	public String getQuemMorreu() {
		return quemMorreu;
	}

	public void setQuemMorreu(String quemMorreu) {
		this.quemMorreu = quemMorreu;
	}

	public String getArmaUsada() {
		return armaUsada;
	}

	public void setArmaUsada(String armaUsada) {
		this.armaUsada = armaUsada;
	}

}
