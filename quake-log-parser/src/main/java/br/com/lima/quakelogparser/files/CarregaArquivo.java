package br.com.lima.quakelogparser.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Observable;

/**
 * 
 * @author Rogério Lima
 *
 */
public class CarregaArquivo extends Observable {

	private static final String ARQUIVO_NAO_ENCONTRADO = "Arquivo não encontrado!";
	private static final String NAO_FOI_POSSIVEL_LER = "Não foi possível ler o arquivo!";
	
	private static List<String> linhas;
	private static long tamanhoDoArquivoCarregado;
	private static Path localDoArquivo;
	
	/**
	 * Busca e le o arquivo passado pelo path
	 * @param path Caminho onde está o arquivo
	 * @return List&lt;String&gt; contendo todas as linhas do arquivo, 
	 * 	onde cada posição da lista armazena uma linha do arquivo.
	 * @throws IOException é lançada caso o arquivo não seja encontrado ou não possa
	 * 	ser lido
	 */
	public static List<String> getLinhas(String path) throws IOException {
		
		localDoArquivo = Paths.get(path);
		boolean arquivoExiste = Files.exists(localDoArquivo);
		
		if (!arquivoExiste) {
			throw new IOException(ARQUIVO_NAO_ENCONTRADO + " : " + path);
		}
		
		if (linhas == null) {
			recarregarArquivo(localDoArquivo);
			
			return linhas;
		}
		
		if (Files.size(localDoArquivo) != tamanhoDoArquivoCarregado) {
			recarregarArquivo(localDoArquivo);
		}

		return linhas;
	}
	
	public static Path getLocalDoArquivoCarregado() {
		return localDoArquivo;
	}
	
	private static void recarregarArquivo(Path localDoArquivo) throws IOException {
		try {
			linhas = Files.readAllLines(localDoArquivo);
			tamanhoDoArquivoCarregado = Files.size(localDoArquivo);
		} catch (IOException e) {
			throw new IOException(NAO_FOI_POSSIVEL_LER + " : " + localDoArquivo);
		}
	}
}
