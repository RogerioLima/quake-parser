package br.com.lima.quakelogparser.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lima.quakelogparser.controllers.GameController;
import br.com.lima.quakelogparser.files.CarregaArquivo;
import br.com.lima.quakelogparser.files.Paths;
import br.com.lima.quakelogparser.model.Game;
import br.com.lima.quakelogparser.model.Player;
import br.com.lima.quakelogparser.resources.exceptions.NaoEncontradoException;

/**
 * 
 * @author Rogério Lima
 *
 */
@RestController
@RequestMapping("/games")
public class GameService {

	@RequestMapping(value = "/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Map<String, Object>> listaTodos() throws Exception {
		Map<String, Object> saida = new HashMap<>();
		try {
			List<String> linhas = CarregaArquivo.getLinhas(Paths.pathLog);
			List<Game> games = new GameController(linhas).listaTodos();

			for (int i = 0; i < games.size(); i++) {
				Game game = games.get(i);
				saida.put(game.getNome(), formataDados(game));
			}
			
		} catch (IOException e) {
			throw new NaoEncontradoException("Arquivo não encontrado : " + Paths.pathLog);
		}
		return new ResponseEntity<Map<String,Object>>(saida, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/game/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Map<String, Object>> buscaGame(@PathVariable("id") int id) throws NaoEncontradoException {
		Map<String, Object> saida = new HashMap<>();
		try {
			List<String> linhas = CarregaArquivo.getLinhas(Paths.pathLog);
			Game game = new GameController(linhas).bucaGame(id);
			if (game == null) {
				throw new NaoEncontradoException("Game " + id + " não encontrado!");
			}
			saida.put(game.getNome(), formataDados(game));
			
		} catch (IOException e) {
			throw new NaoEncontradoException("Arquivo não encontrado : " + Paths.pathLog);
		}
		return new ResponseEntity<Map<String,Object>>(saida, HttpStatus.OK);
	}
	
	private Map<String,Object> formataDados(Game game) {
		Map<String, Object> dados = new HashMap<>();
		List<String> players = new ArrayList<>();
		Map<String, Integer> kills = new HashMap<>();
		Map<String, Integer> suicidios = new HashMap<>();
		
		for (Player player : game.getPlayers()) {
			players.add(player.getNome());
			kills.put(player.getNome(), player.getQuantidadeMortes());
			suicidios.put(player.getNome(), player.getSuicidio());
		}
		
		dados.put("total_kills", game.getTotalKills());
		dados.put("players", players);
		dados.put("kills", kills);
		dados.put("suicides", suicidios);
		dados.put("causes_of_death", game.getCausasDeMorte());
		
		return dados;
	}
	
	@ExceptionHandler(NaoEncontradoException.class)
	public ResponseEntity<Map<String, Object>> handleException(HttpServletRequest req, Exception exception, Model model) {
	    Map<String, Object> saida = new HashMap<>();
	    saida.put("Status", HttpStatus.NOT_FOUND);
	    saida.put("Error Message", exception.getMessage());
	    return new ResponseEntity<Map<String,Object>>(saida, HttpStatus.NOT_FOUND);
	}
	
}
