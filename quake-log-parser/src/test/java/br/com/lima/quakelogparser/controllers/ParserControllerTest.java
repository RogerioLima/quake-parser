package br.com.lima.quakelogparser.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.lima.quakelogparser.files.CarregaArquivo;
import br.com.lima.quakelogparser.files.Paths;
import br.com.lima.quakelogparser.model.Game;
import br.com.lima.quakelogparser.model.Kill;
import br.com.lima.quakelogparser.model.Player;

public class ParserControllerTest {

	private static List<String> linhas;
	private static List<String> linhasSemGames;

	private String linhaInicioDeJogo = " 20:37 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0";
	private String linhaPlayer = " 20:38 ClientUserinfoChanged: 2 n\\Isgalamido\\t\\0\\model\\uriel/zael\\hmodel\\uriel/zael\\g_redteam\\\\g_blueteam\\\\c1\\5\\c2\\5\\hc\\100\\w\\0\\l\\0\\tt\\0\\tl\\0";
	private String linhaDisconnect = " 13:27 ClientDisconnect: 4";
	private String linhaQualquer = "  1:46 Item: 2 item_armor_shard";

	private String linhaKill = " 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT";
	private String linhaKill2 = " 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT";
	private String linhaKill3 = " 22:06 Kill: 2 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH";
	private String linhaKill4 = " 22:18 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH";
	private String linhaKill5 = " 12:24 Kill: 3 4 6: Isgalamido killed Zeh by MOD_ROCKET";
	private String linhaKill6 = " 12:24 Kill: 3 4 6: Isgalamido killed Zeh by MOD_ROCKET_SPLASH";
	private String linhaKill7 = " 14:15 Kill: 2 5 10: Zeh killed Assasinu Credi by MOD_RAILGUN";

	private ParserController parserCompleto;
	private ParserController parserVazio;

	private Player rogerio;
	private Player rogerioComOutroId;
	private Player lima;
	private Player nunes;

	private Game game;

	private List<Player> players;

	@BeforeClass
	public static void initClass() throws IOException {
		linhas = CarregaArquivo.getLinhas(Paths.pathLogTest);
		linhasSemGames = CarregaArquivo.getLinhas(Paths.pathLogTestSemGames);
	}

	@Before
	public void init() {
		parserCompleto = new ParserController(linhas);
		parserVazio = new ParserController(linhasSemGames);
		ParserController.setGames(null);
		ParserController.setTamanhoDoArquivoCarregado(0L);

		rogerio = new Player(1, "Rogério");
		rogerioComOutroId = new Player(7, "Rogério");
		lima = new Player(1, "Lima");
		nunes = new Player(14, "Nunes");

		game = new Game(1);

		players = new ArrayList<>();
		players.add(rogerio);
		players.add(nunes);
	}

	@Test
	public void deveEntenderArquivoSemDadosDeGames() throws IOException {
		List<Game> games = parserVazio.processaGames();
		assertEquals(0, games.size());
	}
	
	@Test
	public void deveProcessarGamesNormalmente() throws IOException {
		List<Game> games = parserCompleto.processaGames();

		assertEquals(3, games.size());

		Game game1 = games.get(0);
		Game game2 = games.get(1);
		Game game3 = games.get(2);

		validaGame1(game1);
		validaGame2(game2);
		validaGame3(game3);
		
	}
	
	private void validaGame1(Game game1) {
		assertEquals("game_1", game1.getNome());
		assertEquals(0, game1.getTotalKills());
		// Players
		assertEquals("Isgalamido", game1.getPlayers().get(0).getNome());
		// Kills
		assertEquals(0, game1.getPlayers().get(0).getQuantidadeMortes());
		// Suicidio
		assertEquals(0, game1.getPlayers().get(0).getSuicidio());
		//Causas de Morte
		assertEquals(0, game1.getCausasDeMorte().size());
	}
	
	private void validaGame2(Game game2) {
		assertEquals("game_2", game2.getNome());
		assertEquals(11, game2.getTotalKills());
		// Players
		assertEquals("Isgalamido", game2.getPlayers().get(0).getNome());
		assertEquals("Dono da Bola", game2.getPlayers().get(1).getNome());
		assertEquals("Mocinha", game2.getPlayers().get(2).getNome());
		// Kills
		assertEquals(-7, game2.getPlayers().get(0).getQuantidadeMortes());
		assertEquals(0, game2.getPlayers().get(1).getQuantidadeMortes());
		assertEquals(0, game2.getPlayers().get(2).getQuantidadeMortes());
		// Suicidio
		assertEquals(2, game2.getPlayers().get(0).getSuicidio());
		assertEquals(0, game2.getPlayers().get(1).getSuicidio());
		assertEquals(0, game2.getPlayers().get(2).getSuicidio());
		//Causas de Morte
		assertEquals(3, game2.getCausasDeMorte().size());
		assertEquals(7, game2.getCausasDeMorte().get("MOD_TRIGGER_HURT").intValue());
		assertEquals(3, game2.getCausasDeMorte().get("MOD_ROCKET_SPLASH").intValue());
		assertEquals(1, game2.getCausasDeMorte().get("MOD_FALLING").intValue());
	}
	
	private void validaGame3(Game game3) {
		assertEquals("game_3", game3.getNome());
		assertEquals(14, game3.getTotalKills());
		// Players
		assertEquals("Dono da Bola", game3.getPlayers().get(0).getNome());
		assertEquals("Isgalamido", game3.getPlayers().get(1).getNome());
		assertEquals("Zeh", game3.getPlayers().get(2).getNome());
		assertEquals("Assasinu Credi", game3.getPlayers().get(3).getNome());
		// Kills
		assertEquals(0, game3.getPlayers().get(0).getQuantidadeMortes());
		assertEquals(2, game3.getPlayers().get(1).getQuantidadeMortes());
		assertEquals(1, game3.getPlayers().get(2).getQuantidadeMortes());
		assertEquals(-1, game3.getPlayers().get(3).getQuantidadeMortes());
		// Suicidio
		assertEquals(0, game3.getPlayers().get(0).getSuicidio());
		assertEquals(0, game3.getPlayers().get(1).getSuicidio());
		assertEquals(0, game3.getPlayers().get(2).getSuicidio());
		assertEquals(2, game3.getPlayers().get(3).getSuicidio());
		//Causas de Morte
		assertEquals(4, game3.getCausasDeMorte().size());
		assertEquals(4, game3.getCausasDeMorte().get("MOD_ROCKET").intValue());
		assertEquals(4, game3.getCausasDeMorte().get("MOD_ROCKET_SPLASH").intValue());
		assertEquals(5, game3.getCausasDeMorte().get("MOD_TRIGGER_HURT").intValue());
		assertEquals(1, game3.getCausasDeMorte().get("MOD_RAILGUN").intValue());
	}

	@Test
	public void atualizaIdOuInseriPlayerNoGame() {
		parserCompleto.atualizaIdOuInserePlayerNoGame(game, rogerio);
		assertEquals(1, game.getPlayers().size());
		assertEquals("Rogério", game.getPlayers().get(0).getNome());
		assertEquals(1, game.getPlayers().get(0).getId());

		parserCompleto.atualizaIdOuInserePlayerNoGame(game, rogerioComOutroId);
		assertEquals(1, game.getPlayers().size());
		assertEquals("Rogério", game.getPlayers().get(0).getNome());
		assertEquals(7, game.getPlayers().get(0).getId());

		parserCompleto.atualizaIdOuInserePlayerNoGame(game, lima);
		assertEquals(2, game.getPlayers().size());
		assertEquals("Lima", game.getPlayers().get(1).getNome());
		assertEquals(1, game.getPlayers().get(1).getId());
	}

	@Test
	public void processaKill() {
		game.getPlayers().add(new Player(1, "Isgalamido"));
		game.getPlayers().add(new Player(2, "Zeh"));

		Kill kill = parserCompleto.retiraKillDaLinha(linhaKill);
		parserCompleto.processaKill(game, kill);

		kill = parserCompleto.retiraKillDaLinha(linhaKill2);
		parserCompleto.processaKill(game, kill);

		kill = parserCompleto.retiraKillDaLinha(linhaKill3);
		parserCompleto.processaKill(game, kill);

		kill = parserCompleto.retiraKillDaLinha(linhaKill4);
		parserCompleto.processaKill(game, kill);

		kill = parserCompleto.retiraKillDaLinha(linhaKill5);
		parserCompleto.processaKill(game, kill);

		kill = parserCompleto.retiraKillDaLinha(linhaKill6);
		parserCompleto.processaKill(game, kill);

		kill = parserCompleto.retiraKillDaLinha(linhaKill7);
		parserCompleto.processaKill(game, kill);

		assertEquals(7, game.getTotalKills());
		
		assertEquals(2, game.getCausasDeMorte().get("MOD_TRIGGER_HURT").intValue());
		assertEquals(3, game.getCausasDeMorte().get("MOD_ROCKET_SPLASH").intValue());
		assertEquals(1, game.getCausasDeMorte().get("MOD_ROCKET").intValue());
		assertEquals(1, game.getCausasDeMorte().get("MOD_RAILGUN").intValue());
		
		Player isgalamido = game.getPlayers().get(0);

		assertEquals(2, isgalamido.getMorriPeloWorld());
		assertEquals(1, isgalamido.getSuicidio());

		assertEquals(3, isgalamido.getQuemMatei().size());

		assertEquals("Mocinha", isgalamido.getQuemMatei().get(0).getQuemMorreu());
		assertEquals("MOD_ROCKET_SPLASH", isgalamido.getQuemMatei().get(0).getArma());

		assertEquals("Zeh", isgalamido.getQuemMatei().get(1).getQuemMorreu());
		assertEquals("MOD_ROCKET", isgalamido.getQuemMatei().get(1).getArma());

		assertEquals("Zeh", isgalamido.getQuemMatei().get(2).getQuemMorreu());
		assertEquals("MOD_ROCKET_SPLASH", isgalamido.getQuemMatei().get(2).getArma());

		Player zeh = game.getPlayers().get(1);
		
		assertEquals(0, zeh.getMorriPeloWorld());
		assertEquals(0, zeh.getSuicidio());

		assertEquals(1, zeh.getQuemMatei().size());

		assertEquals("Assasinu Credi", zeh.getQuemMatei().get(0).getQuemMorreu());
		assertEquals("MOD_RAILGUN", zeh.getQuemMatei().get(0).getArma());
	}

	@Test
	public void desconectaPlayer() {
		game.setPlayers(players);
		parserCompleto.desconectarPlayer(game, 14);
		assertEquals(2, game.getPlayers().size());
		assertEquals(1, game.getPlayers().get(0).getId());
		assertEquals(-1, game.getPlayers().get(1).getId());
	}

	@Test
	public void retiraPlayerDaLinha() {
		Player player = parserCompleto.retiraPlayerDaLinha(linhaPlayer);
		assertEquals(2, player.getId());
		assertEquals("Isgalamido", player.getNome());
	}

	@Test
	public void retiraKillDaLinha() {
		Kill kill = parserCompleto.retiraKillDaLinha(linhaKill);
		assertEquals("<world>", kill.getQuemMatou());
		assertEquals("Isgalamido", kill.getQuemMorreu());
		assertEquals("MOD_TRIGGER_HURT", kill.getArmaUsada());
	}

	@Test
	public void retiraIdDesconectadoDaLinha() {
		int id = parserCompleto.retiraIdDesconectadoDaLinha(linhaDisconnect);
		assertEquals(4, id);
	}

	@Test
	public void linhaDeveSerInicioDeJogo() {
		assertTrue(parserCompleto.isInicioDeJogo(linhaInicioDeJogo));
	}

	@Test
	public void linhaNaoDeveSerInicioDeJogo() {
		assertFalse(parserCompleto.isInicioDeJogo(linhaQualquer));
	}

	@Test
	public void linhaDeveSerPlayer() {
		assertTrue(parserCompleto.isPlayer(linhaPlayer));
	}

	@Test
	public void linhaNaoDeveSerPlayer() {
		assertFalse(parserCompleto.isPlayer(linhaQualquer));
	}

	@Test
	public void linhaDeveSerKill() {
		assertTrue(parserCompleto.isKill(linhaKill));
	}

	@Test
	public void linhaNaoDeveSerKill() {
		assertFalse(parserCompleto.isKill(linhaQualquer));
	}

	@Test
	public void linhaDeveSerDisconnect() {
		assertTrue(parserCompleto.isDisconnect(linhaDisconnect));
	}

	@Test
	public void linhaNaoDeveSerDisconnect() {
		assertFalse(parserCompleto.isDisconnect(linhaQualquer));
	}

}
