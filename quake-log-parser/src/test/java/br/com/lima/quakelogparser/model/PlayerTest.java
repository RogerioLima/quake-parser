package br.com.lima.quakelogparser.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

	private Player player1;
	private Player player2;
	private List<Execucao> mortes1; // total 10
	private List<Execucao> mortes2; // total 13
	
	@Before
	public void init() {
		player1 = new Player(1, "Rogério");
		player2 = new Player(2, "Lima");
		
		mortes1 = new ArrayList<Execucao>();
		mortes1.add(new Execucao());
		mortes1.add(new Execucao());
		mortes1.add(new Execucao());
		
		mortes2 = new ArrayList<Execucao>();
		mortes2.add(new Execucao());
		mortes2.add(new Execucao());
		mortes2.add(new Execucao());
	}
	
	/**
	 * Testa o calculo de mortes do player levando em consideração as vezes em que ele morreu pelo world 
	 */
	@Test
	public void deveEntenderOCalculoDeMortes() {
		
		player1.setMorriPeloWorld(6);
		player1.setQuemMatei(mortes1);
		
		player2.setMorriPeloWorld(17);
		player2.setQuemMatei(mortes2);
		
		assertEquals(-3, player1.getQuantidadeMortes());
		assertEquals(-14, player2.getQuantidadeMortes());
		
	}
}
